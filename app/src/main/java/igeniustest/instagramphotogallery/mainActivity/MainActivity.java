package igeniustest.instagramphotogallery.mainActivity;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import igeniustest.instagramphotogallery.fragments.implementation.callbacks.AttachNewFragment;
import igeniustest.instagramphotogallery.fragments.implementation.CustomFragment;
import igeniustest.instagramphotogallery.R;
import igeniustest.instagramphotogallery.fragments.implementation.callbacks.SetFragmentManager;
import igeniustest.instagramphotogallery.fragments.webViewFragment.WebViewFragment;
import igeniustest.instagramphotogallery.mainActivity.components.ActionBar;
import igeniustest.instagramphotogallery.utils.AlertUtils;
import igeniustest.instagramphotogallery.utils.MethodUtils;

public class MainActivity extends FragmentActivity implements SetFragmentManager, AttachNewFragment {

    private ActionBar actionBar;
    private FragmentManager fragmentManager;
    private FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar = new ActionBar(this);

        if (savedInstanceState == null) {
            attachNewFragment(new WebViewFragment());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!MethodUtils.hasConnection(getBaseContext())) {
            AlertUtils.showWithOneButton(this, alertButtonCLick(), R.string.alert_title_attention, R.string.alert_text_no_connection, R.string.alert_button_close_application);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (actionBar == null) {
            actionBar = new ActionBar(this);
        }
    }

    public void handleBackButton() {
        if (fragmentManager != null) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        handleBackButton();
    }

    @Override
    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void attachNewFragment(CustomFragment fragment) {
        ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.mainFrameFragment, fragment);
        ft.commit();
    }

    @Override
    public void attachNewFragmentNoHistory(CustomFragment prevFragment, CustomFragment nextFragment) {
        ft = getSupportFragmentManager().beginTransaction();
        ft.remove(prevFragment);
        ft.commit();
        attachNewFragment(nextFragment);
    }

    @Override
    public void showActionBar() {
        actionBar.showActionBar();
    }

    @Override
    public void hideActionBar() {
        actionBar.hideActionBar();
    }

    private DialogInterface.OnClickListener alertButtonCLick() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MethodUtils.closeApp();
            }
        };
    }
}
