package igeniustest.instagramphotogallery.mainActivity.components;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import igeniustest.instagramphotogallery.R;
import igeniustest.instagramphotogallery.mainActivity.MainActivity;

/**
 * Created by ivanr on 17/07/2017.
 */

public class ActionBar {

    private MainActivity mainActivity;
    private ImageView actionBarButton;
    private ImageView actionBarTitle;

    public ActionBar(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        init();
    }

    private void init() {
        actionBarTitle = (ImageView) mainActivity.findViewById(R.id.actionBarTitle);

        actionBarButton = (ImageView) mainActivity.findViewById(R.id.actionBarButton);
        actionBarButton.setOnClickListener(actionBarButtonClickListener());
    }

    private View.OnClickListener actionBarButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.handleBackButton();
            }
        };
    }

    public void showActionBar() {
        mainActivity.findViewById(R.id.customActionBar).setVisibility(View.VISIBLE);
    }

    public void hideActionBar() {
        mainActivity.findViewById(R.id.customActionBar).setVisibility(View.GONE);
    }
}
