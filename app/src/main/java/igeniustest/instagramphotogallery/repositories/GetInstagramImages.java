package igeniustest.instagramphotogallery.repositories;

import igeniustest.instagramphotogallery.bodyResponse.Data;
import igeniustest.instagramphotogallery.services.CallExecuter;
import igeniustest.instagramphotogallery.services.CustomCall;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by ivanr on 19/07/2017.
 */

public class GetInstagramImages implements CustomCall {
    private InstagramRecentMedia instagramRecentMedia;
    private String access_token;

    public GetInstagramImages() {
    }

    @Override
    public void prepareCall(Retrofit retrofit) {
        instagramRecentMedia = retrofit.create(InstagramRecentMedia.class);
    }

    @Override
    public Call<Data> executeCall() {
        return instagramRecentMedia.loadDataPhotos(access_token);
    }

    @Override
    public void startCall(Callback callback) {
        CallExecuter.getInstance().run(this, callback);
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
