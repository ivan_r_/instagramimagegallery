package igeniustest.instagramphotogallery.repositories;

import igeniustest.instagramphotogallery.bodyResponse.Data;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ivanr on 19/07/2017.
 */

public interface InstagramRecentMedia {
    @GET("users/self/media/recent/")
    Call<Data> loadDataPhotos(@Query("access_token") String accessToken);
}
