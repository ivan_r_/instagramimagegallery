package igeniustest.instagramphotogallery.services;

import igeniustest.instagramphotogallery.utils.VariableUtils;
import okhttp3.OkHttpClient;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivanr on 17/07/2017.
 */

public class CallExecuter {
    private static final CallExecuter ourInstance = new CallExecuter();
    private final OkHttpClient client = new OkHttpClient();
    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(VariableUtils.INSTAGRAM_API_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static CallExecuter getInstance() {
        return ourInstance;
    }

    private CallExecuter() {
    }

    public void run(CustomCall customCall, Callback callback) {

        customCall.prepareCall(retrofit);

        customCall.executeCall().enqueue(callback);
    }
}
