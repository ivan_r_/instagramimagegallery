package igeniustest.instagramphotogallery.services;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by ivanr on 19/07/2017.
 */

public interface CustomCall {
    void prepareCall(Retrofit retrofit);
    Call executeCall();
    void startCall(Callback callback);
}
