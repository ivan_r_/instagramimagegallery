package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ivanr on 20/07/2017.
 */

public class CarouselMedia extends CompositeMedia implements Parcelable {
    private String type;
    private Images images;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeParcelable(this.images, flags);
    }

    public CarouselMedia() {
    }

    protected CarouselMedia(Parcel in) {
        this.type = in.readString();
        this.images = in.readParcelable(Images.class.getClassLoader());
    }

    public static final Parcelable.Creator<CarouselMedia> CREATOR = new Parcelable.Creator<CarouselMedia>() {
        @Override
        public CarouselMedia createFromParcel(Parcel source) {
            return new CarouselMedia(source);
        }

        @Override
        public CarouselMedia[] newArray(int size) {
            return new CarouselMedia[size];
        }
    };

    @Override
    public Images getCompositeMediaImages() {
        return images;
    }

    @Override
    public String getCompositeMediaType() {
        return type;
    }
}
