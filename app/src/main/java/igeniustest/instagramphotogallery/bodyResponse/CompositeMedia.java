package igeniustest.instagramphotogallery.bodyResponse;

import java.util.List;

/**
 * Created by ivanr on 21/07/2017.
 */

public abstract class CompositeMedia {

    public String getCompositeMediaType() {
        throw new UnsupportedOperationException();
    }

    public Images getCompositeMediaImages() {
        throw new UnsupportedOperationException();
    }

    public void addImagesInList(List<Images> list, String condition) {
        if (condition == null) {
            list.add(getCompositeMediaImages());
        }
        else {
            if (getCompositeMediaType().equals(condition)) {
                list.add(getCompositeMediaImages());
            }
        }
    }
}
