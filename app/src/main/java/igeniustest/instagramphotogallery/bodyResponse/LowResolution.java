package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivanr on 19/07/2017.
 */

public class LowResolution implements Parcelable {
    private String url;
    private int width;
    private int height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    public LowResolution() {
    }

    protected LowResolution(Parcel in) {
        this.url = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<LowResolution> CREATOR = new Parcelable.Creator<LowResolution>() {
        @Override
        public LowResolution createFromParcel(Parcel source) {
            return new LowResolution(source);
        }

        @Override
        public LowResolution[] newArray(int size) {
            return new LowResolution[size];
        }
    };
}
