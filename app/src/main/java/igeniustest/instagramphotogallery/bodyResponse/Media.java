package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivanr on 19/07/2017.
 */

public class Media extends CompositeMedia implements Parcelable {
    private String id;
    private User user;
    private Images images;
    private String created_time;
    private String type;
    private List<CarouselMedia> carousel_media;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }
    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CarouselMedia> getCarousel_media() {
        return carousel_media;
    }

    public void setCarousel_media(List<CarouselMedia> carousel_media) {
        this.carousel_media = carousel_media;
    }

    public Media() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.images, flags);
        dest.writeString(this.created_time);
        dest.writeString(this.type);
        dest.writeTypedList(this.carousel_media);
    }

    protected Media(Parcel in) {
        this.id = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.created_time = in.readString();
        this.type = in.readString();
        this.carousel_media = in.createTypedArrayList(CarouselMedia.CREATOR);
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public String getCompositeMediaType() {
        return type;
    }

    @Override
    public Images getCompositeMediaImages() {
        return images;
    }

    @Override
    public void addImagesInList(List<Images> list, String condition) {
        super.addImagesInList(list, condition);
        if (carousel_media != null) {
            for (CompositeMedia c : carousel_media) {
                c.addImagesInList(list, condition);
            }
        }
    }
}
