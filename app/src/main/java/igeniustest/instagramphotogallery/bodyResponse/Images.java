package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ivanr on 19/07/2017.
 */

public class Images implements Parcelable {
    private LowResolution low_resolution;
    private Thumbnail thumbnail;
    private StandardResolution standard_resolution;

    public LowResolution getLowResolution() {
        return low_resolution;
    }

    public void setLowResolution(LowResolution lowResolution) {
        this.low_resolution = lowResolution;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public StandardResolution getStandardResolution() {
        return standard_resolution;
    }

    public void setStandardResolution(StandardResolution standardResolution) {
        this.standard_resolution = standardResolution;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.low_resolution, flags);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeParcelable(this.standard_resolution, flags);
    }

    public Images() {
    }

    protected Images(Parcel in) {
        this.low_resolution = in.readParcelable(LowResolution.class.getClassLoader());
        this.thumbnail = in.readParcelable(Thumbnail.class.getClassLoader());
        this.standard_resolution = in.readParcelable(StandardResolution.class.getClassLoader());
    }

    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel source) {
            return new Images(source);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
