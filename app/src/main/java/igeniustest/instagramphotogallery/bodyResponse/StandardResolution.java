package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivanr on 19/07/2017.
 */

public class StandardResolution implements Parcelable {

    private String url;
    private int width;
    private int height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    public StandardResolution() {
    }

    protected StandardResolution(Parcel in) {
        this.url = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<StandardResolution> CREATOR = new Parcelable.Creator<StandardResolution>() {
        @Override
        public StandardResolution createFromParcel(Parcel source) {
            return new StandardResolution(source);
        }

        @Override
        public StandardResolution[] newArray(int size) {
            return new StandardResolution[size];
        }
    };
}
