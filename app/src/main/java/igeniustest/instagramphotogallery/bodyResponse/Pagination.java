package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ivanr on 23/07/2017.
 */

public class Pagination implements Parcelable {
    private int next_max_tag_id;
    private String deprecation_warning;
    private int next_max_id;
    private int next_min_id;
    private int min_tag_id;
    private String next_url;
    private int max_id;
    private int min_id;

    public Pagination() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.next_max_tag_id);
        dest.writeString(this.deprecation_warning);
        dest.writeInt(this.next_max_id);
        dest.writeInt(this.next_min_id);
        dest.writeInt(this.min_tag_id);
        dest.writeString(this.next_url);
        dest.writeInt(this.max_id);
        dest.writeInt(this.min_id);
    }

    protected Pagination(Parcel in) {
        this.next_max_tag_id = in.readInt();
        this.deprecation_warning = in.readString();
        this.next_max_id = in.readInt();
        this.next_min_id = in.readInt();
        this.min_tag_id = in.readInt();
        this.next_url = in.readString();
        this.max_id = in.readInt();
        this.min_id = in.readInt();
    }

    public static final Creator<Pagination> CREATOR = new Creator<Pagination>() {
        @Override
        public Pagination createFromParcel(Parcel source) {
            return new Pagination(source);
        }

        @Override
        public Pagination[] newArray(int size) {
            return new Pagination[size];
        }
    };
}
