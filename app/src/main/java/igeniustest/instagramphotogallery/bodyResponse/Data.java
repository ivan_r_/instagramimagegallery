package igeniustest.instagramphotogallery.bodyResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ivanr on 19/07/2017.
 */

public class Data implements Parcelable {
    private List<Media> data;
    private Pagination pagination;

    public List<Media> getData() {
        return data;
    }

    public void setData(List<Media> data) {
        this.data = data;
    }

    public Data() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
        dest.writeParcelable(this.pagination, flags);
    }

    protected Data(Parcel in) {
        this.data = in.createTypedArrayList(Media.CREATOR);
        this.pagination = in.readParcelable(Pagination.class.getClassLoader());
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
