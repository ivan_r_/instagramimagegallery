package igeniustest.instagramphotogallery.fragments.implementation.callbacks;

import android.support.v4.app.FragmentManager;

/**
 * Created by ivanr on 18/07/2017.
 */

public interface SetFragmentManager {
    void setFragmentManager(FragmentManager fragmentManager);
}
