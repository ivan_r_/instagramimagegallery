package igeniustest.instagramphotogallery.fragments.implementation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import igeniustest.instagramphotogallery.fragments.implementation.callbacks.UpdateUICallback;
import igeniustest.instagramphotogallery.fragments.implementation.callbacks.AttachNewFragment;
import igeniustest.instagramphotogallery.fragments.implementation.callbacks.SetFragmentManager;

/**
 * Created by ivanr on 18/07/2017.
 */

public class CustomFragment extends Fragment implements UpdateUICallback {

    public SetFragmentManager setFragmentManager;
    public AttachNewFragment attachNewFragment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setFragmentManager = (SetFragmentManager) context;
        attachNewFragment = (AttachNewFragment) context;
    }

    public void attachFragmentManager() {
        setFragmentManager.setFragmentManager(this.getFragmentManager());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        attachFragmentManager();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void updateSuccessUI(Object object) {

    }

    @Override
    public void updateFailUI(Object object) {

    }
}
