package igeniustest.instagramphotogallery.fragments.implementation.callbacks;

import igeniustest.instagramphotogallery.fragments.implementation.CustomFragment;

/**
 * Created by ivanr on 18/07/2017.
 */

public interface AttachNewFragment {
    void attachNewFragment(CustomFragment fragment);
    void attachNewFragmentNoHistory(CustomFragment prevFragment, CustomFragment nextfragment);
    void showActionBar();
    void hideActionBar();
}
