package igeniustest.instagramphotogallery.fragments.implementation.callbacks;

/**
 * Created by ivanr on 20/07/2017.
 */

public interface UpdateUICallback {
    void updateSuccessUI(Object object);
    void updateFailUI(Object object);
}
