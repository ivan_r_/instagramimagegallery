package igeniustest.instagramphotogallery.fragments.webViewFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import igeniustest.instagramphotogallery.R;
import igeniustest.instagramphotogallery.fragments.photoGalleryFragment.PhotoGalleryFragment;
import igeniustest.instagramphotogallery.fragments.implementation.CustomFragment;
import igeniustest.instagramphotogallery.fragments.webViewFragment.components.WebViewFragmentManager;
import igeniustest.instagramphotogallery.utils.VariableUtils;

/**
 * Created by ivanr on 18/07/2017.
 */

public class WebViewFragment extends CustomFragment {

    private WebView webView;
    private String accessToken;
    private SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview, container, false);

        webView = (WebView) view.findViewById(R.id.loginWebView);
        sharedPreferences = WebViewFragment.this.getActivity().getSharedPreferences(VariableUtils.SHARED_PREF_ID, Context.MODE_PRIVATE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                if (url.contains("access_token")) {
                    accessToken = url.substring(url.indexOf("access_token=")).replace("access_token=", "");

                    sharedPreferences.edit().putString(VariableUtils.SHARED_PREF_USER_TOKEN, accessToken).apply();

                    navigateNext();
                }
            }
        });

        accessToken = sharedPreferences.getString(VariableUtils.SHARED_PREF_USER_TOKEN, null);
        if (accessToken == null) {
            webView.loadUrl("https://api.instagram.com/oauth/authorize/?client_id="
                    + VariableUtils.INSTRAGRAM_CLIENT_ID
                    + "&redirect_uri="
                    + VariableUtils.INSTRAGRAM_REDIRECT_URL
                    + "&response_type=token");
        }
        else {
            navigateNext();
        }
        return view;
    }

    private void navigateNext() {
        CustomFragment fragment = new PhotoGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(VariableUtils.SHARED_PREF_USER_TOKEN, accessToken);
        fragment.setArguments(bundle);
        attachNewFragment.attachNewFragmentNoHistory(this, fragment);
    }

    @Override
    public void attachFragmentManager() {
        setFragmentManager.setFragmentManager(new WebViewFragmentManager(webView));
    }
}
