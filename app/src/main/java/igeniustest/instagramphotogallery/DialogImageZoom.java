package igeniustest.instagramphotogallery;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import igeniustest.instagramphotogallery.fragments.photoGalleryFragment.components.ImageDiskCache;
import igeniustest.instagramphotogallery.fragments.photoGalleryFragment.components.ThumbnailCache;
import igeniustest.instagramphotogallery.utils.MethodUtils;
import igeniustest.instagramphotogallery.utils.VariableUtils;

/**
 * Created by ivanr on 22/07/2017.
 */

public class DialogImageZoom extends DialogFragment {
    private String thumbUrl;
    private String standardUrl;
    private ImageView img;
    private String imgName;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (savedInstanceState == null) {
            thumbUrl = getArguments().getString(VariableUtils.BUNDLE_CURRENT_THUMBNAIL_IMG);
            standardUrl = getArguments().getString(VariableUtils.BUNDLE_CURRENT_STANDARD_IMG);
        }
        else {
            thumbUrl = savedInstanceState.getString(VariableUtils.BUNDLE_CURRENT_THUMBNAIL_IMG);
            standardUrl = savedInstanceState.getString(VariableUtils.BUNDLE_CURRENT_STANDARD_IMG);
        }

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(VariableUtils.BUNDLE_CURRENT_THUMBNAIL_IMG, thumbUrl);
        outState.putString(VariableUtils.BUNDLE_CURRENT_STANDARD_IMG, standardUrl);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;

            Window window = dialog.getWindow();
            window.setLayout(width, height);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            img = new ImageView(getContext());
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            img.setLayoutParams(params);
            img.setScaleType(ImageView.ScaleType.FIT_CENTER);
            int padding = MethodUtils.pxToDp(getContext(), 25);
            img.setPadding(padding, padding, padding, padding);
            img.setOnClickListener(dismissClick(this));

            if (ThumbnailCache.getInstance().getThumbnail(thumbUrl) != null) {
                img.setImageBitmap(ThumbnailCache.getInstance().getThumbnail(thumbUrl));
            }
            else {
                img.setImageResource(R.mipmap.placeholder);
            }

            window.addContentView(img, params);

            imgName = MethodUtils.getImageNameFromUrl(standardUrl);
            ImageDiskCache.getInstance().getImageFromCallback(target, imgName, standardUrl, getContext());
        }
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            img.setImageBitmap(bitmap);
            ImageDiskCache.getInstance().saveImageOnDisk(imgName, bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    private View.OnClickListener dismissClick(final DialogFragment fragment) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.dismiss();
            }
        };
    }
}
