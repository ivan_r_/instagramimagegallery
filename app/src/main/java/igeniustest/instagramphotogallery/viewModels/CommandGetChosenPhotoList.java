package igeniustest.instagramphotogallery.viewModels;

import java.util.ArrayList;
import java.util.List;

import igeniustest.instagramphotogallery.bodyResponse.CompositeMedia;
import igeniustest.instagramphotogallery.bodyResponse.Images;
import igeniustest.instagramphotogallery.bodyResponse.Media;

/**
 * Created by ivanr on 20/07/2017.
 */

public class CommandGetChosenPhotoList {
    private List<CompositeMedia> medias;
    private List<Images> imagesList;
    private String lastChosen = "";

    public CommandGetChosenPhotoList(List<Media> data) {
        medias = (List<CompositeMedia>) (List) data;
    }

    public List<Images> getChosenPhotoList(String mediaChosenType) {
        if (imagesList == null || !lastChosen.equals(mediaChosenType)) {
            lastChosen = mediaChosenType;

            imagesList = new ArrayList<>();
            for (CompositeMedia c: medias) {
                c.addImagesInList(imagesList, mediaChosenType);
            }
        }
        return imagesList;
    }
}
