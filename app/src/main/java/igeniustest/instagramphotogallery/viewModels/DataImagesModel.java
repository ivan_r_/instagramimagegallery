package igeniustest.instagramphotogallery.viewModels;

import android.app.ProgressDialog;
import android.content.DialogInterface;

import igeniustest.instagramphotogallery.DialogImageZoom;
import igeniustest.instagramphotogallery.R;
import igeniustest.instagramphotogallery.fragments.implementation.CustomFragment;
import igeniustest.instagramphotogallery.repositories.GetInstagramImages;
import igeniustest.instagramphotogallery.fragments.implementation.callbacks.UpdateUICallback;
import igeniustest.instagramphotogallery.bodyResponse.Data;
import igeniustest.instagramphotogallery.utils.AlertUtils;
import igeniustest.instagramphotogallery.utils.MethodUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ivanr on 20/07/2017.
 */

public class DataImagesModel implements Callback {
    private Data dataImages;
    private GetInstagramImages getInstagramImages;
    private UpdateUICallback updateUICallback;
    private ProgressDialog progressDialog;

    public DataImagesModel(UpdateUICallback updateUICallback) {
        this.updateUICallback = updateUICallback;
        if (getInstagramImages == null) {
            getInstagramImages = new GetInstagramImages();
        }
    }

    public void init(String access_token, CustomFragment context) {
        if (dataImages != null) {
            updateUICallback.updateSuccessUI(dataImages);
            return;
        }
        getInstagramImages.setAccess_token(access_token);

        if (MethodUtils.hasConnection(context.getContext())) {
            progressDialog = AlertUtils.progressDialog(context.getContext(), R.string.alert_loader_title, R.string.alert_loader_text, true);
            progressDialog.show();

            getInstagramImages.startCall(this);
        }
        else {

        }
    }

    public Data getDataImages() {
        return dataImages;
    }

    @Override
    public void onResponse(Call call, Response response) {
        dataImages = (Data) response.body();
        updateUICallback.updateSuccessUI(dataImages);
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        progressDialog.dismiss();
        updateUICallback.updateFailUI(call);
    }
}
