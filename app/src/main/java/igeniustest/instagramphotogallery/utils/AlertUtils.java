package igeniustest.instagramphotogallery.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by ivanr on 18/10/2016.
 */

public class AlertUtils {

    private static AlertDialog.Builder builder;

    private static void initAlert(Context ctx){
        builder = new AlertDialog.Builder(ctx);
    }

    public static void show(Context ctx, String title, String message, boolean isCancelable){
        initAlert(ctx);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(isCancelable);
        builder.show();
    }

    public static void showWithOneButton(Context ctx, DialogInterface.OnClickListener clickListener, int titleIdRes, int messageIdRes, int buttonStringIdRes){
        initAlert(ctx);
        builder.setTitle(ctx.getResources().getString(titleIdRes));
        builder.setMessage(ctx.getResources().getString(messageIdRes));
        builder.setCancelable(false);
        builder.setPositiveButton(buttonStringIdRes, clickListener);
        builder.show();
    }

    public static ProgressDialog progressDialog(Context ctx, int titleIdRes, int messageIdRes, boolean indeterminate) {
        ProgressDialog dialog = ProgressDialog.show(ctx, ctx.getResources().getString(titleIdRes), ctx.getResources().getString(messageIdRes), indeterminate);
        return dialog;
    }
}
