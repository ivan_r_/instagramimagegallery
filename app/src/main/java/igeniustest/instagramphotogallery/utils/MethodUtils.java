package igeniustest.instagramphotogallery.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by ivanr on 22/07/2017.
 */

public final class MethodUtils {
    /*
        GRAPHIC
     */
    public static int pxToDp(Context context, float px) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (px * density + 0.5f);
    }


    /*
        UTILS
     */
    public static String getImageNameFromUrl(String url) {
        String imgName = null;
        int a = url.indexOf(".jpg") - 1;
        for (int i=a; ; i--) {
            if (url.charAt(i) == '/') {
                imgName = url.substring(i+1, a + 5);
                break;
            }
        }
        return imgName;
    }

    public static void closeApp() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }


    /*
        NETWORK
     */
    public static boolean hasConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
