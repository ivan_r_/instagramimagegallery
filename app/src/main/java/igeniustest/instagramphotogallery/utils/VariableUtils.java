package igeniustest.instagramphotogallery.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by ivanr on 17/07/2017.
 */

public final class VariableUtils {
    /*
        HTTP & WEB
     */
    public static final String INSTRAGRAM_CLIENT_ID = "0f0d1bbb50044451a3f75d758d5b4802";
    public static final String INSTRAGRAM_REDIRECT_URL = "https://www.google.com";
    public static final String INSTAGRAM_API_BASE_URL = "https://api.instagram.com/v1/";


    /*
        FRAGMENTS TAGS
     */
    public static final String FRAGMENT_TAG_DIALOG_ZOOM = "dialog_zoom_fragment";


    /*
        SHARED PREFERENCES
     */
    public static final String SHARED_PREF_ID = "instagram_photo_gallery";
    public static final String SHARED_PREF_USER_TOKEN = "user_token";


    /*
        DATA
     */
    public static final String DATA_VIDEO_TYPE_IMAGES = "video";
    public static final String DATA_PHOTO_TYPE_IMAGES = "image";
    public static final String DATA_ALL_TYPE_IMAGE = null;


    /*
        BUNDLE VARIABLES
     */
    public static final String BUNDLE_CURRENT_THUMBNAIL_IMG = "thumpKey";
    public static final String BUNDLE_CURRENT_STANDARD_IMG = "standardKey";

    public static final String BUNDLE_MEDIA_DATA_LIST = "dataImagesModel";


    /*
        STORAGE & FILE
     */
    public static final String FILE_BASE_INTERNAL_URL_NAME = Environment.getExternalStorageDirectory() + File.separator + "InstagramGallery";
}
