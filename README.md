# readme
L' applicativo si basa su di una singola activity (che funge da Dispatcher) ed un paio di fragments, aggiunti all' interno di un FrameLayout nella MainActivity.
I due fragment ereditano da una implementazione base.
All' interno dell' implementazione base vi sono delle logiche centralizzate (settaggio di un paio di callback) e settaggio (overridable) del FragmentManager (che verrà poi utilizzato dalla main activity sulla base dello Strategy Pattern)
Per quanto riguarda l' architettura dell' applicativo, dato che non ho mai avuto a che fare con solide architetture app, ho deciso di mettere su un M-V-VM basandomi sulle linee guida Google.
Il risultato dell' architettura, a mio parere, non è sicuramente perfetto, risluta un pò macchinoso e non del tutto flessibile.
Per quanto riguarda lo zoom ho deciso di utilizzare un FragmentDialog, così da avere una UX più immediata.
Il salvataggio foto si basa su una cache in memoria (per le thumbnail) e una cache su disco (per le immagine a risoluzione standard).
Per poter visualizzare anche i media annidati ho deciso di utilizzare il Composite Pattern all' interno di un Command così da seprare completamente la logica dal modello.

Librerie utilizzate:
OkHttp (primo utilizzo),
RetroFit (primo utilizzo),
Picasso (primo utilizzo)

I motivi del non utilizzo di RxJava, se ci sarà occasione, sarò felice di spiegarli.